﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.DTO
{
    public class UserDto
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }
    }
}
