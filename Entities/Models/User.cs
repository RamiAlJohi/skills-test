﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities.Models
{
    public class User : ModelBase
    {
        [Required(ErrorMessage = "First Name is required")]
        [StringLength(30, ErrorMessage = "First Name Can't be longer than 30 characters")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name is required")]
        [StringLength(30, ErrorMessage = "Last Name Can't be longer than 30 characters")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Mobile is required")]
        [StringLength(30, ErrorMessage = "Last Name Can't be longer than 30 characters")]
        public string Mobile { get; set; }
        
        [Required(ErrorMessage = "Email is required")]
        [StringLength(30, ErrorMessage = "Last Name Can't be longer than 30 characters")]
        public string Email { get; set; }
    }
}
