﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities
{
    public class ModelBase
    {
        [Key]
        public Guid Id { get; set; }

        public DateTime CreatedDate { get; set; }

        public Guid? CreatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public Guid? UpdatedBy { get; set; }

        public bool IsActive { get; set; }


        protected ModelBase()
        {
            Id = Guid.NewGuid();
            CreatedDate = DateTime.Now.ToLocalTime();
            IsActive = true;
        }
    }
}
