﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions<RepositoryContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(
                new User()
                {
                    Id = Guid.Parse("d28888e9-2ba9-473a-a40f-e38cb54f9b35"),
                    CreatedDate = DateTime.Parse("Dec 8, 2020"),
                    FirstName = "Rami",
                    LastName = "Salem",
                    Mobile = "0534663364",
                    Email = "ramisalem95@gmail.com"
                }
            );

            base.OnModelCreating(modelBuilder);
        }
    }
}
