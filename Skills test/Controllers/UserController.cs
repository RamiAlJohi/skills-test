﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts;
using Entities.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Skills_test.Controllers
{
    /// <summary>
    /// This is a RESTFull API version of Home Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IRepositoryWrapper _repositoryWrapper;
        private readonly IMapper _mapper;

        public UserController(ILogger<HomeController> logger, IRepositoryWrapper repositoryWrapper, IMapper mapper)
        {
            _logger = logger;
            _repositoryWrapper = repositoryWrapper;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            List<User> users = await _repositoryWrapper.User.FindAll().ToListAsync();
            List<UserDto> usersDto = _mapper.Map<List<UserDto>>(users);
            return Ok(usersDto);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            User user = await _repositoryWrapper.User.FindByCondition(x => x.Id == id).FirstAsync();
            UserDto userDto = _mapper.Map<UserDto>(user);
            return Ok(userDto);
        }


        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserAddDto userAddDto)
        {
            User user = _mapper.Map<User>(userAddDto);
            _repositoryWrapper.User.Create(user);

            if (!TryValidateModel(user, nameof(user)))
            {
                return RedirectToAction("Index");
            }

            await _repositoryWrapper.Save();
            UserDto userDto = _mapper.Map<UserDto>(user);

            return CreatedAtAction(nameof(userDto), new { id = userDto.Id }, userDto);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] UserAddDto userAddDto)
        {
            User user = await _repositoryWrapper.User.FindByCondition(x => x.Id == id).FirstAsync();
            user.FirstName = userAddDto.FirstName;
            user.LastName = userAddDto.LastName;
            user.Email = userAddDto.Email;
            user.Mobile = userAddDto.Mobile;

            if (!TryValidateModel(user, nameof(user)))
            {
                return RedirectToAction("Index");
            }

            _repositoryWrapper.User.Update(user);
            await _repositoryWrapper.Save();
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            User user = await _repositoryWrapper.User.FindByCondition(x => x.Id == id).FirstAsync();
            _repositoryWrapper.User.Delete(user);
            await _repositoryWrapper.Save();
            return Ok();
        }
    }
}
