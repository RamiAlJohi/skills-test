﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts;
using Entities.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Skills_test.Models;

namespace Skills_test.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IRepositoryWrapper _repositoryWrapper;
        private readonly IMapper _mapper;

        public HomeController(ILogger<HomeController> logger, IRepositoryWrapper repositoryWrapper, IMapper mapper)
        {
            _logger = logger;
            _repositoryWrapper = repositoryWrapper;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            List<User> users = await _repositoryWrapper.User.FindAll().ToListAsync();
            List<UserDto> usersDto = _mapper.Map<List<UserDto>>(users);

            return View(usersDto);
        }

        public async Task<IActionResult> Add()
        {
            List<User> users = await _repositoryWrapper.User.FindAll().ToListAsync();
            List<UserDto> usersDto = _mapper.Map<List<UserDto>>(users);

            return View(usersDto);
        }

        public async Task<IActionResult> Update()
        {
            List<User> users = await _repositoryWrapper.User.FindAll().ToListAsync();
            List<UserDto> usersDto = _mapper.Map<List<UserDto>>(users);

            return View(usersDto);
        }

        public async Task<IActionResult> Remove()
        {
            List<User> users = await _repositoryWrapper.User.FindAll().ToListAsync();
            List<UserDto> usersDto = _mapper.Map<List<UserDto>>(users);

            return View(usersDto);
        }


        public async Task<IActionResult> AddUser(UserAddDto userAddDto)
        {
            User user = _mapper.Map<User>(userAddDto);
            _repositoryWrapper.User.Create(user);

            if (!TryValidateModel(user, nameof(user)))
            {
                return RedirectToAction("Index");
            }

            await _repositoryWrapper.Save();
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> UpdateUser(UserDto userDto)
        {
            User user = await _repositoryWrapper.User.FindByCondition(x => x.Id == userDto.Id).FirstAsync();
            user.FirstName = userDto.FirstName;
            user.LastName = userDto.LastName;
            user.Email = userDto.Email;
            user.Mobile = userDto.Mobile;

            if (!TryValidateModel(user, nameof(user)))
            {
                return RedirectToAction("Index");
            }

            _repositoryWrapper.User.Update(user);
            await _repositoryWrapper.Save();
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> RemoveUser(Guid id)
        {
            User user = await _repositoryWrapper.User.FindByCondition(x => x.Id == id).FirstAsync();
            _repositoryWrapper.User.Delete(user);
            await _repositoryWrapper.Save();
            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
